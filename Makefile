#
# Makefile for Linux
#

SYNCDIR = ~/sync/server/games/Z-Machine/zxzvm

INC   = in_zxzvm.inc in_wrhex.inc in_ver.inc
ZXZVM = vm.zsm vmvar.zsm vm0ops.zsm vm1ops.zsm vm2ops.zsm vmvops.zsm \
        vmeops.zsm vmarith.zsm vmobj.zsm vmprop.zsm vmzchar.zsm vmdebug.zsm \
	vmdict.zsm

NXINC = nxdefs.inc nxmacros.inc
NXIO = nxzxzvm.zsm nxdep.zsm nx80.zsm nxinput.zsm nxio.zsm nxp3dos.zsm
NXSUPPORT = nxzxzvm/nxzxzvm.bas nxzxzvm/zxzvm-next.doc \
	    nxzxzvm/fonts/fontgen.bas nxzxzvm/zxzvm.doc
NXFONTS = nxzxzvm/fonts/normal.fnt nxzxzvm/fonts/emphasis.fnt \
	  nxzxzvm/fonts/accent.fnt nxzxzvm/fonts/accentem.fnt \
	  nxzxzvm/fonts/font3.fnt nxzxzvm/fonts/font3em.fnt \
	  nxzxzvm/fonts/italic.fnt nxzxzvm/fonts/underline.fnt \
	  nxzxzvm/fonts/accent_italic.fnt nxzxzvm/fonts/accent_underline.fnt

all:	nxzxzvm.bin

nxzxzvm.bin: ${NXIO} ${NXINC} ${ZXZVM} ${INC}
	pasmo --nocase --equ ZJT=06800h --equ VMORG=07000h nxzxzvm.zsm $@ nxzxzvm.sym
	cp nxzxzvm.bin zxzvm.doc copying nxzxzvm/
	grep -i space_available nxzxzvm.sym

sync: nxzxzvm.bin
	mkdir -p $(SYNCDIR)
	rsync -cvr nxzxzvm/ $(SYNCDIR)/

clean:
	rm -f *.sym nxzxzvm.bin nxzxzvm/nxzxzvm.bin nxzxzvm/zxzvm.doc nxzxzvm/copying
