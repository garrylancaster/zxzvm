;    ZXZVM: Z-Code interpreter for the Z80 processor
;    Copyright (C) 1998-9,2006,2016  John Elliott <seasip.webmaster@gmail.com>
;
;    Updates copyright (C) 2022 Garry Lancaster <garrylancaster@gmail.com>
;    Updates copyright (C) 2021-2022 Shawn Sijnstra <shawn@sijnstra.com>
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

; 2022-04-18, Garry Lancaster (based on work by Shawn Sijnstra in m4zvm):
;               Replaced existing PRNG with 16-bit xorshift
;               Corrected stepped random numbers to start at 1
;               Multiple optimisations

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Arithmetic operations:
;
;neg* : Calculate 2's complement of a 16-bit register
;abs* : Ensure a 16-bit register is +ve
;
abshl:  bit     7,h
        ret     z
neghl:  ex      af,af'
        xor     a
        sub     l
        ld      l,a
        sbc     a,a
        sub     h
        ld      h,a
        ex      af,af'
        ret
;
absde:  bit     7,d
        ret     z
negde:  ex      af,af'
        xor     a
        sub     e
        ld      e,a
        sbc     a,a
        sub     d
        ld      d,a
        ex      af,af'
        ret
;
absbc:  bit     7,b
        ret     z
negbc:  ex      af,af'
        xor     a
        sub     c
        ld      c,a
        sbc     a,a
        sub     b
        ld      b,a
        ex      af,af'
        ret
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Signed 16-bit comparison
;
cpsign:
;
;If HL < DE, return Carry set. If HL >= DE, return Carry clear. If HL=DE
;return Zero set.
;
;Check if one is -ve and the other positive
;
        ld      a,h
        xor     d       ;If the sign bits were different, bit 7 of A is set
        jp      m,dsign
;
;Signs are the same.
;
; << v1.01 Do not reverse the comparison if both values are negative.
;         (In 2s-complement, 0FFFFh (-1) > 0FFFDh (-3) )
;
;        So just fall through to the unsigned comparison.
; >>
;
cphlde:
cpusgn: ld      a,h
        cp      d
        ret     nz
        ld      a,l
        cp      e
        ret
;
cpdebc: ld      a,d
        cp      b
        ret     nz
        ld      a,e
        cp      c
        ret
;
dsign:  bit     7,h     ;Is HL the negative one?
        scf
        ret     nz      ;If so, HL < DE. Return NZ C
ncnz:   xor     a
        inc     a       ;Force NZ
        ret             ;Otherwise, HL >= DE. Return NZ NC
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Signed 16-bit multiplication, BC * DE -> DE
;
m16w:   ld      a,b
        xor     d
        jp      m,mixmult       ;-ve * +ve
        call    absde
        call    absbc
        jr      umult16
;
mixmult:
        call    absde
        call    absbc
        call    umult16
        jp      negde
;
;Unsigned 16-bit multiplication, HLDE := BC * DE
;
umult16:
        ld      hl,0
        ld      a,16    ;16-bit multiplication
umulta: bit     0,e
        jr      z,umultb
        add     hl,bc
umultb: srl     h
        rr      l
        rr      d
        rr      e
        dec     a
        jp      nz,umulta
        ret
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Compute BC mod DE, returns result in HL.
;
smod16: push    de
        push    bc
        call    mo16w
        pop     bc
        pop     de
        ret
;
mo16w:  bit     7,d
        call    nz,negde;a mod (-b) == a mod b
        bit     7,b     ;(-a) mod b == - (a mod b)
        jr      z,udiv16
        call    absbc
        call    udiv16
        jp      neghl

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;Divide BC by DE, returns result in BC.
;
d16w:   ld      a,b             ;Same sign, or opposite signs?
        xor     d
        jp      m,mixdiv
        call    absde
        call    absbc
        jr      udiv16

mixdiv: call    absde
        call    absbc
        call    udiv16
        ret     nc
        call    negbc
        scf
        ret
;
udiv16: ld      a,d     ;Divides BC by DE. Gives result in BC, remainder in HL.
        or      e
        ret     z       ;Return NC if dividing by 0
        ld      hl,0
        ld      a,16
udiv1:  scf
        rl      c
        rl      b
        adc     hl,hl
        sbc     hl,de
        jr      nc,udiv2
        add     hl,de
        dec     c
udiv2:  dec     a
        jr      nz,udiv1
        scf
        ret
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;<< v0.03 overhauled
;
;
;Random number generator...
;
rmode:  defb    0       ;0: pseudo-random  1:counting 1-n
rseed:  defw    42      ;non-zero placeholder
rlast:  defw    0
;
;Seed with a randomish number
;
srandr: call    ZXRNDI  ;Get some randomish number
        ld      a,r
        xor     d
        ld      d,a     ;DE = randomish number
        or      e
        jr      nz,srandr2      ;ensure seed is never 0
        inc     de
srandr2:
        ld      (rseed),de
        xor     a
        ld      (rmode),a
        ld      (rlast),de
        ret
;
;Seed with a fixed number (-HL)
;
srand:  call    neghl           ;make seed positive
        ld      (rseed),hl
        ld      de,1000
        call    cphlde
        ld      a,1
        jr      c,srand1
        inc     a
srand1: ld      (rmode),a
        ld      hl,(rseed)
        dec     hl              ;adjusted because we increment before returning a value.
        ld      (rlast),hl
        ret
;
random: bit     7,h             ;If HL negative, set predictable mode
        jr      nz,srand        ;
        ld      a,h             ;If HL 0, seed from random number
        or      l
        jr      z,srandr
        ld      a,(rmode)       ;Mode 1: Generate random number by stepping
        dec     a
        jr      z,steprnd

;
; 16-bit xorshift pseudorandom number generator
; returns   hl = pseudorandom number
;
        ex      de,hl           ;DE = max value (0 < result <= DE)
        ld      hl,(rseed)
        ld      a,h
        rra
        ld      a,l
        rra
        xor     h
        ld      h,a
        ld      a,l
        rra
        ld      a,h
        rra
        xor     l
        ld      l,a
        xor     h
        ld      h,a
        ld      (rseed),hl
        ld      b,h
        ld      c,l
;
;BC = number (16-bit). Reduce modulo parameter
;
        res     7,b
        call    udiv16
        inc     hl              ;<<v0.03>> value is 0 < n <= max
                                ;          not      0 <=n < max
        scf
        ret                     ;Return value in HL.



steprnd:
        push    hl
        ld      bc,(rlast)      ;Stepping 1,2, ... seed
        inc     bc
;
;BC = number candidate
;
        ld      de,(rseed)
        call    smod16          ;BC := BC mod (seed)
        ld      (rlast),hl
        pop     de
        ld      b,h
        ld      c,l
        res     7,b
        call    mo16w           ;Reduce mod parameter
        inc     hl              ;0 < x <= param
        scf
        ret
